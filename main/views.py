from rest_framework import generics, status, views
from rest_framework.response import Response
from .models import Client, Mailing, Message
from django.db.models import Count
from .serializers import ClientSerializer, MailingSerializer, MessageSerializer
from django.utils import timezone
from .tasks import start_mailing
import logging

logger = logging.getLogger(__name__)

#############################################
class ClientCreateAPIView(generics.CreateAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer

    def perform_create(self, serializer):
        serializer.save()
        logger.info(f"Client, ID = {serializer.data.get('id', None)}, {serializer.data} succesfully added")


class ClientRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer

    def perform_update(self, serializer):
        serializer.save()
        logger.info(f"Client, ID = {serializer.data.get('id', None)}, {serializer.data} succesfully edited")


###############################################
class MailingCreateAPIView(generics.CreateAPIView):
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer

    def perform_create(self, serializer):
        mailing = serializer.save()
        logger.info(f"Mailing, ID = {serializer.data.get('id', None)}, {serializer.data} succesfully added")
        if  mailing.start_datetime > timezone.now():
            # Start mailing automatically when start time is in the future
            start_mailing.apply_async(args=[mailing.id], eta=mailing.start_datetime)
        else:
            start_mailing(mailing.id)


class MailingListAPIView(generics.ListAPIView):
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer


class MailingRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer

    def perform_update(self, serializer):
        serializer.save()
        logger.info(f"Mailing, ID = {serializer.data.get('id', None)}, {serializer.data} succesfully edited")



class MailingStatsView(views.APIView):
    def get(self, request):
        # Get total statistics for all mailings
        total_stats = Message.objects.values('mailing__id', 'mailing__name', 'status')\
            .annotate(count=Count('id'))
        # Get detailed statistics for a specific mailing
        mailing_id = request.query_params.get('mailing_id')
        if mailing_id:
            detailed_stats = Message.objects.filter(mailing__id=mailing_id)\
                .values('status')\
                .annotate(count=Count('id'))
            return Response({'total_stats': total_stats, 'detailed_stats': detailed_stats})
        return Response({'total_stats': total_stats})

class MailingDetailStatsAPIView(views.APIView):
    def get(self, request, mailing_id):
        mailing = Mailing.objects.filter(id=mailing_id).first()
        if not mailing:
            return Response({'message': 'Mailing not found.'}, status=404)
        num_sent_messages = mailing.messages.filter(status=Message.SENT).count()
        num_pended_messages = mailing.messages.filter(status=Message.PENDING).count()
        num_failed_messages = mailing.messages.filter(status=Message.FAILED).count()
        stats = {
            'num_sent_messages': num_sent_messages,
            'num_pended_messages': num_pended_messages,
            'num_failed_messages': num_failed_messages,
            'total_messages': num_sent_messages + num_pended_messages + num_failed_messages,
        }
        return Response(stats)

#################################################

class MessageListAPIView(generics.ListAPIView):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer