from django.urls import path
from .views import (
    ClientCreateAPIView, ClientRetrieveUpdateDestroyAPIView,
    MailingCreateAPIView, MailingListAPIView, MailingRetrieveUpdateDestroyAPIView,
    MessageListAPIView, MailingStatsView, MailingDetailStatsAPIView
)

urlpatterns = [
    path('clients/', ClientCreateAPIView.as_view(), name='client_create'),
    path('clients/<int:pk>/', ClientRetrieveUpdateDestroyAPIView.as_view(), name='client_detail'),
    path('mailings/', MailingCreateAPIView.as_view(), name='mailing_create'),
    path('mailings/', MailingListAPIView.as_view(), name='mailing_list'),
    path('mailings/<int:pk>/', MailingRetrieveUpdateDestroyAPIView.as_view(), name='mailing_detail'),
    path('mailings_stats', MailingStatsView.as_view(), name='mailing_detail'),
    path('mailings_stats/<int:pk>/', MailingDetailStatsAPIView.as_view(), name='mailing_detail'),
    path('messages/', MessageListAPIView.as_view(), name='message_list'),
]