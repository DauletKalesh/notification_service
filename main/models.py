from django.db import models
from django.utils import timezone
from django.core.exceptions import ValidationError
import re
from .service import send_message




class Client(models.Model):

    def validate_phone_number(value):
        """
        Validator for the phone_number field in the Client model. 
        Ensures that the phone number is a valid Russian mobile phone number.
        """
        if not re.match(r'^7\d{10}$', value):
            raise ValidationError('Invalid phone number format. Phone number must be in the format 7XXXXXXXXXX')
    
    phone_number = models.CharField(max_length=11, validators=[validate_phone_number])
    operator_code = models.CharField(max_length=3)
    tag = models.CharField(max_length=255)
    timezone = models.CharField(max_length=255)


class Message(models.Model):
    SENT = 'sent'
    PENDING = 'pending'
    FAILED = 'failed'

    STATUS_CHOICES = [
        (SENT, 'Sent'),
        (PENDING, 'Pending'),
        (FAILED, 'Failed'),
    ]

    created_at = models.DateTimeField(default=timezone.now)
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default=PENDING)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    mailing = models.ForeignKey('Mailing', on_delete=models.CASCADE)


class Mailing(models.Model):
    start_datetime = models.DateTimeField()
    end_datetime = models.DateTimeField()
    message_text = models.TextField()
    operator_code_filter = models.CharField(max_length=3)

    def is_active(self):
        now = timezone.now()
        return self.start_datetime <= now <= self.end_datetime

    def get_matching_clients(self):
        clients = Client.objects.all()
        if self.operator_code_filter:
            clients = clients.filter(operator_code=self.operator_code_filter)
        return clients

    def send_messages(self, clients):
        for client in clients:
            message = Message.objects.create(
                mailing=self,
                client=client,
                status=Message.SENT,
            )
            
            if not send_message(
                id = message.id,
                phone = int(client.phone_number),
                text = self.message_text
            ):
                message.status = Message.FAILED
            message.save()