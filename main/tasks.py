from celery import shared_task
from .models import Mailing

@shared_task
def start_mailing(mailing_id):
    mailing = Mailing.objects.get(id=mailing_id)
    if mailing.is_active():
        matching_clients = mailing.get_matching_clients()
        mailing.send_messages(matching_clients)

