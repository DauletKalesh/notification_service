import requests
import logging
logger = logging.getLogger('main.views')


def send_message(id: int, phone: int, text: str):
    
    data = {
        'id' : id,
        'phone' : phone,
        'text' : text
    }
    url = f'https://probe.fbrq.cloud/v1/send/{id}'

    headers = {
        'accept': 'application/json',
        'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MTUzNDIwNzgsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6IjE5QjA1MDU0NiJ9.tW320iiimsaFrhuRNEdhfVewmCXxfJfir4P9h7zlCvg',
        'Content-Type': 'application/json'
    }

    response = requests.post(url, headers=headers, json=data)
    logger.info(f'OpenAPI, request: {data = }, response: {response.json() = }')
    if response.status_code == 200:
        return True
    return False