# Документация сервиса уведомления (Основная задача)

В данной документации представлен обзор сервиса рассылки, который обрабатывает отправку сообщений клиентам на основе заданных правил. Сервис позволяет управлять клиентами, создавать и управлять рассылками, а также отслеживать статистику отправленных сообщений. Также в него интегрирован внешний сервис доставки сообщений через OpenAPI.

### Разработан через **DjangoRestFramework, PostgreSQL, Celery, Redis** 
![DjangoRestFramework Version](https://img.shields.io/badge/djangorestframework-3.0.0-orange)
![PostgreSQL Version](https://img.shields.io/badge/postgre-3.0.0-blue.svg)
![Celery Version](https://img.shields.io/badge/celery-3.0.0-green.svg)
![Redis Version](https://img.shields.io/badge/redis-3.0.0-red.svg)

| Таблица: Client         |                            |
|-------------------------|----------------------------|
| Поле                    | Тип данных                 |
|-------------------------|----------------------------|
| id                      | Уникальный идентификатор    |
| phone_number            | Строка (макс. 11 символов) |
| operator_code           | Строка (макс. 3 символа)   |
| tag                     | Строка (макс. 255 символов) |
| timezone                | Строка (макс. 255 символов) |

| Таблица: Message        |                            |
|-------------------------|----------------------------|
| Поле                    | Тип данных                 |
|-------------------------|----------------------------|
| id                      | Уникальный идентификатор    |
| created_at              | Дата/время                 |
| status                  | Строка (макс. 10 символов) |
| client_id               | Внешний ключ (Client)      |
| mailing_id              | Внешний ключ (Mailing)     |

| Таблица: Mailing        |                            |
|-------------------------|----------------------------|
| Поле                    | Тип данных                 |
|-------------------------|----------------------------|
| id                      | Уникальный идентификатор    |
| start_datetime          | Дата/время                 |
| end_datetime            | Дата/время                 |
| message_text            | Текстовое поле             |
| operator_code_filter    | Строка (макс. 3 символа)   |


### URL pattern endpoints:
```python
urlpatterns = [
    path('clients/', ClientCreateAPIView.as_view(), name='client_create'),
    path('clients/<int:pk>/', ClientRetrieveUpdateDestroyAPIView.as_view(), name='client_detail'),
    path('mailings/', MailingCreateAPIView.as_view(), name='mailing_create'),
    path('mailings/', MailingListAPIView.as_view(), name='mailing_list'),
    path('mailings/<int:pk>/', MailingRetrieveUpdateDestroyAPIView.as_view(), name='mailing_detail'),
    path('mailings_stats', MailingStatsView.as_view(), name='mailing_detail'),
    path('mailings_stats/<int:pk>/', MailingDetailStatsAPIView.as_view(), name='mailing_detail'),
    path('messages/', MessageListAPIView.as_view(), name='message_list'),
]
```
### Реализовано отправка сообщении через OpenAPI путь файле
> main/service.py

## Запуск проекта

#### Clone the repository to your local machine:
>`git clone https://gitlab.com/DauletKalesh/notification_service.git`

#### To run the project:

>`docker-compose up --build`



# Дополнительная задача
#### Пункты:
(3) Реализовано docker-compose

(12) Реализовано логирование